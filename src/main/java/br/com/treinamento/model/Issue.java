package br.com.treinamento.model;

import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author caio
 */
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Issue extends AbstractEntity {

    private Long issueNumber;
    private String title;
    private String description;

    @OneToMany(mappedBy = "issueId")
    private Collection<Borrowing> lendings;

    public Issue(Long issueNumber, String title, String description) {
        this.issueNumber = issueNumber;
        this.title = title;
        this.description = description;
    }

    public Issue() {
    }

    public Long getIssueNumber() {
        return issueNumber;
    }

    public void setIssueNumber(Long issueNumber) {
        this.issueNumber = issueNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Borrowing> getLendings() {
        return lendings;
    }

    public void setLendings(Collection<Borrowing> lendings) {
        this.lendings = lendings;
    }
}
