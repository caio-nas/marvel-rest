package br.com.treinamento.model;

import java.util.Date;
import javax.persistence.Entity;

/**
 * @author caio
 */
@Entity
public class Borrowing extends AbstractEntity {

    private Long personId;
    private Long issueId;
    private Date startDate;
    private Date endDate;

    public Borrowing() {
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getIssueId() {
        return issueId;
    }

    public void setIssueId(Long issueId) {
        this.issueId = issueId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
