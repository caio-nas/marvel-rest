package br.com.treinamento.model;

import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Person extends AbstractEntity {

    private String firstName;
    private String lastName;

    @OneToMany(mappedBy = "personId")
    private Collection<Borrowing> borrowings;

    public Person() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Collection<Borrowing> getBorrowings() {
        return borrowings;
    }

    public void setBorrowings(Collection<Borrowing> borrowings) {
        this.borrowings = borrowings;
    }
}
