package br.com.treinamento.model.repository;

import br.com.treinamento.model.Issue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "collection", path = "collection")
public interface IssueRepository extends JpaRepository<Issue, Long> {

}
