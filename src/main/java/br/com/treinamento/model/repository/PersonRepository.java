package br.com.treinamento.model.repository;

import java.util.List;
import br.com.treinamento.model.Person;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "people", path = "people")
public interface PersonRepository extends JpaRepository<Person, Long> {
    List<Person> findByLastName(@Param("name") String name);
}
