package br.com.treinamento.model.repository;

import br.com.treinamento.model.Issue;
import org.springframework.stereotype.Component;
import br.com.treinamento.service.MarvelAPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

/**
 * @author caio
 */
@Component
@RepositoryEventHandler(Issue.class)
public class IssueRepositoryEventHandler {

    @Autowired
    private MarvelAPIService marvelService;

    @HandleBeforeCreate
    public void handleIssueCreate(Issue i) {
        if (i.getId() != null) {
            Issue loadedIssue = marvelService.getComic(i.getId());
            if(loadedIssue != null){
                i.setDescription(loadedIssue.getDescription());
                i.setTitle(loadedIssue.getTitle());
                i.setIssueNumber(loadedIssue.getIssueNumber());
            }
        }
    }
}
