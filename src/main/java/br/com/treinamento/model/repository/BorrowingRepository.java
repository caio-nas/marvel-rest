package br.com.treinamento.model.repository;

import br.com.treinamento.model.Borrowing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface BorrowingRepository extends JpaRepository<Borrowing, Long> {

}
