package br.com.treinamento.service;

import java.util.Date;
import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.stream.Stream;
import java.security.MessageDigest;
import java.util.stream.Collectors;
import br.com.treinamento.model.Issue;
import java.security.NoSuchAlgorithmException;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;
import org.springframework.web.client.RestTemplate;
import org.springframework.data.domain.PageRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Spider Man = 1009610
 *
 * @author caio
 */
@Service
public class MarvelAPIService {

    private String publicKey, privateKey, apiRoot;
    private ObjectMapper objectMapper;

    @Autowired
    public MarvelAPIService(@Value("${marvel.publicKey}") String publicKey,
                            @Value("${marvel.privateKey}") String privateKey,
                            @Value("${marvel.apiRoot}") String apiRoot,
                            ObjectMapper objectMapper) {

        this.apiRoot = apiRoot;
        this.publicKey = publicKey;
        this.privateKey = privateKey;
        this.objectMapper = objectMapper;
    }

    public MarvelAPIService() {
    }

    public Issue getComic(Long id) {
        final String urlTemplate = "%s/comics/%s?ts=%s&apikey=%s&hash=%s";
        final String ts = String.valueOf(new Date().getTime());
        final String hash = createAuthHash(ts);
        final String target = String.format(urlTemplate,
                apiRoot, id, ts, publicKey, hash);

        return getSingleResult(target, Issue.class);
    }

    public List<Issue> findAllComics() {
        return findAllComics(new PageRequest(0, 0));
    }

    public List<Issue> findAllComics(Pageable pageable) {
        final String urlTemplate = "%s/comics?ts=%s&apikey=%s&hash=%s&limit=%s&offset=%s";
        final String ts = String.valueOf(new Date().getTime());
        final String hash = createAuthHash(ts);
        final String target = String.format(urlTemplate,
                apiRoot, ts, publicKey, hash, pageable.getPageSize(), pageable.getPageNumber());

        return getList(target, Issue.class);
    }

    private <T> List<T> getList(String targetUrl, Class<T> cls) {
        final Stream<T> pojos = getResultStream(targetUrl, cls);
        return pojos.collect(Collectors.toList());
    }

    private <T> T getSingleResult(String targetUrl, Class<T> cls) {
        final Stream<T> pojos = getResultStream(targetUrl, cls);
        return pojos.findFirst().orElse(null);
    }

    private <T> Stream<T> getResultStream(String targetUrl, Class<T> cls) {
        final RestTemplate restTemplate = new RestTemplate();
        final Map jsonResponse = restTemplate.getForObject(targetUrl, Map.class);

        final Map data = (Map) jsonResponse.getOrDefault("data", new HashMap());
        final List<T> items = (List) data.getOrDefault("results", new ArrayList<>());
/*
        Com stream seria:
        Stream stream = jsonResponse.entrySet().stream()
                .filter(entry -> "data".equals(entry.getKey()))
                .flatMap(entry -> Stream.of(((Map) entry.getValue())))
                .flatMap(data -> ((List)  data.get("results")).stream())
                .map(i -> objectMapper.convertValue(i, cls));
*/
        return items.stream().map(i -> objectMapper.convertValue(i, cls));
    }

    private String createAuthHash(final String ts) {
        final String hashBase = ts + privateKey + publicKey;
        final MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            throw new IllegalArgumentException(ex);
        }

        return (new HexBinaryAdapter()).marshal(md5.digest(hashBase.getBytes())).toLowerCase();
    }
}
