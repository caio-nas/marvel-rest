package br.com.treinamento.controller;

import java.util.List;
import br.com.treinamento.model.Issue;
import org.springframework.data.domain.Pageable;
import br.com.treinamento.service.MarvelAPIService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/comics/marvel")
public class MarvelComicsController {

    @Autowired
    private MarvelAPIService marvelService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Issue> findAllComics(Pageable pageable) {
        return marvelService.findAllComics(pageable);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Issue getComic(@PathVariable Long id) {
        return marvelService.getComic(id);
    }
}
