package br.com.treinamento.model.repository;

import org.junit.Test;
import org.junit.Assert;
import org.mockito.Mockito;
import org.junit.runner.RunWith;
import br.com.treinamento.model.Issue;
import br.com.treinamento.service.MarvelAPIService;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author caio
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class IssueRepositoryEventHandlerTests {

    private Issue handleIssueCreate(Long localIssueId, Long remoteIssueId, Issue serviceResponse){
        final IssueRepositoryEventHandler handler = new IssueRepositoryEventHandler();
        final MarvelAPIService service = Mockito.mock(MarvelAPIService.class);
        final Issue originalIssue = new Issue(4545L, "Local comic", "No description");
        originalIssue.setId(localIssueId);

        Mockito.when(service.getComic(remoteIssueId)).thenReturn(serviceResponse);
        ReflectionTestUtils.setField(handler, "marvelService", service);

        handler.handleIssueCreate(originalIssue);
        return originalIssue;
    }

    @Test
    public void shouldCreateLocalEntity() throws Exception {
        Issue i = handleIssueCreate(111L, 999L, new Issue(3434L, "Loaded comic", "Lorem Ipsum Dolor..."));

        Assert.assertEquals(new Long(4545), i.getIssueNumber());
        Assert.assertEquals("Local comic", i.getTitle());
        Assert.assertEquals("No description", i.getDescription());
    }

    @Test
    public void shouldCreateLocalEntityIfRequestReturnsNull() throws Exception {
        Issue i = handleIssueCreate(999L, 999L, null);

        Assert.assertEquals(new Long(4545), i.getIssueNumber());
        Assert.assertEquals("Local comic", i.getTitle());
        Assert.assertEquals("No description", i.getDescription());
    }

    @Test
    public void shouldCreateLoadedEntity() throws Exception {
        Issue i = handleIssueCreate(999L, 999L, new Issue(3434L, "Loaded comic", "Lorem Ipsum Dolor..."));

        Assert.assertEquals(new Long(3434), i.getIssueNumber());
        Assert.assertEquals("Loaded comic", i.getTitle());
        Assert.assertEquals("Lorem Ipsum Dolor...", i.getDescription());
    }
}
