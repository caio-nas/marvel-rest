package br.com.treinamento.model;

import org.junit.Test;
import br.com.treinamento.SpringRestTestCase;
import static org.hamcrest.Matchers.containsString;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class PersonTests extends SpringRestTestCase {

    @Test
    public void shouldReturnRepositoryIndex() throws Exception {
        mockMvc.perform(get("/"))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$._links.people").exists());
    }

    @Test
    public void shouldCreateEntity() throws Exception {
        mockMvc.perform(post("/people")
               .content("{\"firstName\": \"Frodo\", \"lastName\":\"Baggins\"}"))
               .andExpect(status().isCreated())
               .andExpect(header().string("Location", containsString("people/")));
    }

    @Test
    public void shouldRetrieveEntity() throws Exception {
        MvcResult mvcResult = createPerson();

        final String location = mvcResult.getResponse().getHeader("Location");
        mockMvc.perform(get(location))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.firstName").value("Frodo"))
               .andExpect(jsonPath("$.lastName").value("Baggins"));
    }

    @Test
    public void shouldQueryEntity() throws Exception {
        createPerson();

        mockMvc.perform(get("/people/search/findByLastName?name={name}", "Baggins"))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$._embedded.people[0].firstName")
               .value("Frodo"));
    }

    @Test
    public void shouldUpdateEntity() throws Exception {
        final MvcResult mvcResult = createPerson();
        final String location = mvcResult.getResponse().getHeader("Location");

        mockMvc.perform(put(location)
               .content("{\"firstName\": \"Bilbo\", \"lastName\":\"Jr.\"}"))
               .andExpect(status().isNoContent());

        mockMvc.perform(get(location))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.firstName").value("Bilbo"))
               .andExpect(jsonPath("$.lastName").value("Jr."));
    }

    @Test
    public void shouldPartiallyUpdateEntity() throws Exception {
        final MvcResult mvcResult = createPerson();
        final String location = mvcResult.getResponse().getHeader("Location");

        mockMvc.perform(patch(location).content("{\"firstName\": \"Bilbo Jr.\"}"))
               .andExpect(status().isNoContent());

        mockMvc.perform(get(location))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.firstName").value("Bilbo Jr."))
               .andExpect(jsonPath("$.lastName").value("Baggins"));
    }

    @Test
    public void shouldDeleteEntity() throws Exception {
        final MvcResult mvcResult = createPerson();
        final String location = mvcResult.getResponse().getHeader("Location");

        mockMvc.perform(delete(location)).andExpect(status().isNoContent());
        mockMvc.perform(get(location)).andExpect(status().isNotFound());
    }

    private MvcResult createPerson() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/people")
                                     .content("{\"firstName\": \"Frodo\", \"lastName\":\"Baggins\"}"))
                                     .andExpect(status().isCreated())
                                     .andReturn();

        return mvcResult;
    }
}
