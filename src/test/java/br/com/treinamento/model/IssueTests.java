package br.com.treinamento.model;

import org.junit.Test;
import br.com.treinamento.SpringRestTestCase;
import static org.hamcrest.Matchers.containsString;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class IssueTests extends SpringRestTestCase {

    @Test
    public void shouldReturnRepositoryIndex() throws Exception {
        mockMvc.perform(get("/"))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$._links.collection").exists());
    }

    @Test
    public void shouldCreateEntity() throws Exception {
        mockMvc.perform(post("/collection")
               .content("{\"issueNumber\": \"1244\", \"title\":\"Avengers 10\", \"description\":\"test\"}"))
               .andExpect(status().isCreated())
               .andExpect(header().string("Location", containsString("collection/")));
    }

    @Test
    public void shouldRetrieveEntity() throws Exception {
        final MvcResult mvcResult = createIssue();
        final String location = mvcResult.getResponse().getHeader("Location");

        mockMvc.perform(get(location))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.issueNumber").value("1244"))
               .andExpect(jsonPath("$.title").value("Avengers 10"))
               .andExpect(jsonPath("$.description").value("test"));
    }


    @Test
    public void shouldUpdateEntity() throws Exception {
        final MvcResult mvcResult = createIssue();
        final String location = mvcResult.getResponse().getHeader("Location");

        mockMvc.perform(put(location)
               .content("{\"issueNumber\": \"1255\", \"title\":\"Avengers 21\", \"description\":\"A more detailed description\"}"))
               .andExpect(status().isNoContent());

        mockMvc.perform(get(location))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.issueNumber").value("1255"))
               .andExpect(jsonPath("$.title").value("Avengers 21"))
               .andExpect(jsonPath("$.description").value("A more detailed description"));
    }

    @Test
    public void shouldPartiallyUpdateEntity() throws Exception {
        final MvcResult mvcResult = createIssue();
        final String location = mvcResult.getResponse().getHeader("Location");

        mockMvc.perform(patch(location).content("{\"title\": \"Spider Man\"}"))
               .andExpect(status().isNoContent());

        mockMvc.perform(get(location))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.issueNumber").value("1244"))
               .andExpect(jsonPath("$.title").value("Spider Man"))
               .andExpect(jsonPath("$.description").value("test"));
    }

    @Test
    public void shouldDeleteEntity() throws Exception {
        final MvcResult mvcResult = createIssue();
        final String location = mvcResult.getResponse().getHeader("Location");

        mockMvc.perform(delete(location)).andExpect(status().isNoContent());
        mockMvc.perform(get(location)).andExpect(status().isNotFound());
    }

    private MvcResult createIssue() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/collection")
                                     .content("{\"issueNumber\": \"1244\", \"title\":\"Avengers 10\", \"description\":\"test\"}"))
                                     .andExpect(status().isCreated())
                                     .andReturn();

        return mvcResult;
    }
}
