package br.com.treinamento.model;

import org.junit.Test;
import br.com.treinamento.SpringRestTestCase;
import static org.hamcrest.Matchers.containsString;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class BorrowingTests extends SpringRestTestCase {

    @Test
    public void shouldReturnRepositoryIndex() throws Exception {
        mockMvc.perform(get("/"))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$._links.borrowings").exists());
    }

    @Test
    public void shouldCreateEntity() throws Exception {
        mockMvc.perform(post("/collection")
               .content("{\"issueId\":\"1\", \"personId\":\"1\",\"startDate\":\"2016-10-21\",\"endDate\":\"2016-10-26\"}"))
               .andExpect(status().isCreated())
               .andExpect(header().string("Location", containsString("collection/")));
    }

    @Test
    public void shouldRetrieveEntity() throws Exception {
        final MvcResult mvcResult = createBorrowing();
        final String location = mvcResult.getResponse().getHeader("Location");

        mockMvc.perform(get(location))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.issueId").value("1"))
               .andExpect(jsonPath("$.personId").value("1"))
               .andExpect(jsonPath("$.startDate").value("2016-10-21T00:00:00.000+0000"))
               .andExpect(jsonPath("$.endDate").value("2016-10-26T00:00:00.000+0000"));
    }


    @Test
    public void shouldUpdateEntity() throws Exception {
        final MvcResult mvcResult = createBorrowing();
        final String location = mvcResult.getResponse().getHeader("Location");

        mockMvc.perform(put(location)
               .content("{\"issueId\":\"2\", \"personId\":\"2\",\"startDate\":\"2017-10-21\",\"endDate\":\"2017-10-26\"}"))
               .andExpect(status().isNoContent());

        mockMvc.perform(get(location))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.issueId").value("2"))
               .andExpect(jsonPath("$.personId").value("2"))
               .andExpect(jsonPath("$.startDate").value("2017-10-21T00:00:00.000+0000"))
               .andExpect(jsonPath("$.endDate").value("2017-10-26T00:00:00.000+0000"));
    }

    @Test
    public void shouldPartiallyUpdateEntity() throws Exception {
        final MvcResult mvcResult = createBorrowing();
        final String location = mvcResult.getResponse().getHeader("Location");

        mockMvc.perform(patch(location).content("{\"endDate\": \"2022-10-26\"}"))
               .andExpect(status().isNoContent());

        mockMvc.perform(get(location))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.issueId").value("1"))
               .andExpect(jsonPath("$.personId").value("1"))
               .andExpect(jsonPath("$.startDate").value("2016-10-21T00:00:00.000+0000"))
               .andExpect(jsonPath("$.endDate").value("2022-10-26T00:00:00.000+0000"));
    }

    @Test
    public void shouldDeleteEntity() throws Exception {
        final MvcResult mvcResult = createBorrowing();
        final String location = mvcResult.getResponse().getHeader("Location");

        mockMvc.perform(delete(location)).andExpect(status().isNoContent());
        mockMvc.perform(get(location)).andExpect(status().isNotFound());
    }

    private MvcResult createBorrowing() throws Exception {
        mockMvc.perform(post("/people")
               .content("{\"firstName\": \"Frodo\", \"lastName\":\"Baggins\"}"));
        mockMvc.perform(post("/people")
               .content("{\"firstName\": \"Bilbo\", \"lastName\":\"Baggins\"}"));

        mockMvc.perform(post("/collection")
               .content("{\"issueNumber\": \"1244\", \"title\":\"Return of the king\", \"description\":\"test1\"}"));
        mockMvc.perform(post("/collection")
               .content("{\"issueNumber\": \"1245\", \"title\":\"The Hobbit\", \"description\":\"test2\"}"));

        MvcResult mvcResult = mockMvc.perform(post("/borrowings")
                .content("{\"issueId\":\"1\", \"personId\":\"1\",\"startDate\":\"2016-10-21\",\"endDate\":\"2016-10-26\"}"))
                .andExpect(status().isCreated())
                .andReturn();

        return mvcResult;
    }
}
